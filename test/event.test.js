// const { Model, DataTypes } = require("sequelize");
// import { Model, DataTypes } from "sequelize";
const express = require('express');
const request = require('supertest');
const eventsRouter = require("../src/routes/events");
const { Op } = require("sequelize");

require("dotenv").config();

const db = require('../src/db');

const { runServer } = require('../src/server');

// afterEach(async () => {
//     const test_events = await db.models.Event
//         .findAll({
//             where: {
//                 [Op.iLike]: `%TEST%`
//             }
//         });
//         // console.log(test_events)
//     // for (const e of test_events) {
//     //     await e.destroy()
//     // }
// });

test('it should create an event', async () => {

    const PORT = 5555;
    const app = express();

    app.set("db", db);

    await app.get("db").sequelize.sync();

    app.use(express.json());
    app.use("/events", eventsRouter);

    const server = runServer(PORT, app);

    const req_data = {
        start_date: Date.now(),
        end_date: Date.now(),
        description: "TEST"
    };

    const res = await request(app)
        .post("/events/test")
        .send(req_data)
        .set("Content-Type", "application/json")
        .set('Accept', 'application/json')
        .catch(err => console.log(err));

    expect(res).toBeDefined();
    expect(res.statusCode).toEqual(200);
    
    (async function() {await db.sequelize.sync(e)})();
    // const created_data = res.body.created;
    // expect(created_data.description).toBe("my birthday");

    // const created = await Event.findOne(created_data.id);
    // expect(created).not.toBeNull();
    server.close(); // en attendant mieux
});