require("dotenv").config();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
//var validator = require('validator');
const { check, validationResult } = require('express-validator');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var eventsRouter = require("./routes/events");
var todolistsRouter = require("./routes/todolists");
var registriesRouter = require("./routes/registries");

const db = require("./db");

const rootdir = path.dirname(__dirname);

var app = express();

(async function() {await db.sequelize.sync()})();

app.set("db", db);
app.set('views', path.join(rootdir, "src", 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(validator());
// app.use(require('connect-flash'));
app.use(express.static(path.join(rootdir, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use("/events", eventsRouter);
app.use("/todolist", todolistsRouter);
app.use("/registry", registriesRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;