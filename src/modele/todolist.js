const { Sequelize, Model, DataTypes } = require("sequelize");



class Todolist extends Model {}
Todolist.init({

    id: {
        type: DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    name: {
        type: DataTypes.STRING,
        len: [2, 55]
    },
    description: {
        type: DataTypes.STRING,
        len: [2, 500]
    },
    state: {
        type: DataTypes.BOOLEAN,
    },
    
});



