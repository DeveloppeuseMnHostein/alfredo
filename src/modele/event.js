const { Sequelize, Model, DataTypes } = require('sequelize');
//const sequelize = new Sequelize('sqlite::postgres:');

class EventToRec extends Model { }
EventToRec.init({
    Name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    Description: {
        type: DataTypes.STRING,
        len: [2, 255]
    },
    Date_begin: {
        type: DataTypes.DATE,
        allowNull: false,
        validate: {
            isDate: {
                msg: "This must be a date"
            },
        },
    },
    Date_ending: {
        type: DataTypes.DATE,
        allowNull: false,
        validate: {
            isDate: {
                msg: "This must be a date"
            },
        },
    },
    Location: {
        type: DataTypes.STRING,
    },
}, 
    //    { sequelize, modelName:'user' });
);


(async () => {
    await sequelize.sync();
    const jane = await User.create({
        username: 'janedoe',
        birthday: new Date(1980, 6, 20)
    });
    console.log(jane.toJSON());
})();