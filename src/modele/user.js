const cookieParser = require("cookie-parser");
const { DataTypes, Validator } = require("sequelize");
const { default: isEmail } = require("validator/lib/isEmail");


class User extends Model {

    //Set up the associations so we can make the queries that include
    //the related object.
    static associate({ AuthToken }) {
        User.hasMany(AuthToken);
    }

    //This is a class method, it is not called on an individual user
    //object, but rather the class as a whole.
    static async authenticate(username, password) {
        const user = await User.findOne({ where: { username } });

        //bcrypt is a one-way hashing algorithm that allows us to store
        //string on the db rather than the raw password.
        if (bcrypt.compareSync(password, user.password)) {
            return user.authorize();
        }
        throw new Error('Invalid password');
    }

    //In order to define an instance method, we have to access the UserModele prototype.
    async authorize() {
        const { AuthToken } = sequelize.models;
        const user = this

        //Create a new authToken associated to "this" user by calling the AuthToken class
        //method we created earlier and passing it to the user id 
        const authToken = await AuthToken.generate(this.id);

        //Add AuthToken is a generated method provided by sequelize with :
        await user.addAuthToken(authToken);

        return { user, authToken }
    }

    async logout(token) {

        //destroy the auth token record that matches the passed token
        sequelize.models.authToken({ where: { token } });
    }
}


module.exports = sequelize => {
    User.init({

        email: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false,
            validate: isEmail
        },
        username: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false,
            len: [2, 55]
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        },
    }, { sequelize });

    // //Set up the associations so we can make the queries that include
    // //the related object.
    // User.associate = function ({ AuthToken }) {
    //     User.hasMany(AuthToken);
    // };



    // //This is a class method, it is not called on an individual user
    // //object, but rather the class as a whole.
    // User.authenticate = async function (username, password) {
    //     const user = await User.findOne({ where: { username } });

    //     //bcrypt is a one-way hashing algorithm that allows us to store
    //     //string on the db rather than the raw password.
    //     if (bcrypt.compareSync(password, user.password)) {
    //         return user.authorize();
    //     }
    //     throw new Error('Invalid password');
    // };


    return User;
}
