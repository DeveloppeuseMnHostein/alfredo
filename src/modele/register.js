const { Sequelize, Model, DataTypes } = require('sequelize');


class Registry extends Model {}
Registry.init({
    
    id: {
        type: DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    name: {
        type: DataTypes.STRING,
        len: [2, 55]
    },
    description: {
        type: DataTypes.STRING,
        len: [2, 500]
    },
});
