const sequelize = require("../db")
const event = require("./event");
const registry = require("./registry");
const todolist = require("./todolist");

const models = {
    Event: event(sequelize),
    Todolist: todolist(sequelize),
    Registry: registry(sequelize),
    
};

module.exports = models;