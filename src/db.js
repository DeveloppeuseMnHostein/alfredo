const { Sequelize } = require("sequelize");
const { PGUSER, PGPASSWORD, PGPORT, PGDATABASE, PGHOST } = process.env;

const sequelize = new Sequelize(`postgres://${PGUSER}:${PGPASSWORD}@${PGHOST}:${PGPORT}/${PGDATABASE}`);

const db = {
    sequelize,
    models: {
        Event: require("./modele/event")(sequelize),
        //index: require("./modele/index")(sequelize),
        Todolist: require("./modele/todolist")(sequelize),
        Registry: require("./modele/registry")(sequelize),
    }
};

module.exports = db;