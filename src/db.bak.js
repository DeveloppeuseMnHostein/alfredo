
// //Exemple avec sequelize.define
// const User = sequelize.define('User', {
//     //Model attributes are defined here
//     firstName: {
//         type: DataTypes.STRING,
//         allowNull: false,
//         validate: {
//             notEmpty: true
//         }
//     },
//     lastName: {
//         type: DataTypes.STRING
//         //allowNull defaults to true
//     },
//     birthday: {
//         type: DataTypes.DATE
//     },
//     picture: {
//         type: Sequelize.BLOB('long')
//     },

// }, {
//     tableName: 'Users'
// });








// //Exemple avec Extend
// class UserAccount extends Model { }

// User.init({
//     username: DataTypes.STRING,
//     userSurname: DataTypes.STRING,
//     mail: DataTypes.STRING,
//     birthday: DataTypes.DATE,
// }, { sequelize, modelName: 'user' });

// (async () => {
//     await sequelize.sync();
//     const jane = await User.create({
//         username: 'Jane',
//         userSurname: 'Doe',
//         mail: 'janedoe@gmail.com',
//         birthday: new Date(1980, 6, 20)
//     });
//     console.log(jane.toJSON());
// })();

