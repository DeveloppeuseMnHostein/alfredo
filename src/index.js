#!/usr/bin/env node

/**
 * Module dependencies.
 */

var app = require('./app');
const { runServer } = require('./server');

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT;

runServer(port, app);
