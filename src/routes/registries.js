var express = require('express');

var router = express.Router();


router.get('/registry', async function (req, res, next) {
    const { Registry } = req.app.get("db").models;

    const results = await Registry.findAllById();
    res.json(results);
});

module.exports = router;