var express = require('express');
const todolist = require('../modele/todolist');

var router = express.Router();


router.get('/todolist', async function (req, res, next) {
    const { Todolist } = req.app.get("db").models;

    const results = await Todolist.findAll();
    res.json(results);
});

module.exports = router;
