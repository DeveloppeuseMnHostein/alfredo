var express = require('express');

var router = express.Router();

router.post("/test", function (req, res) {
    return res.json({test: req.body})
});

router.get('/all', async function (req, res, next) {
    const { Event } = req.app.get("db").models;

    const results = await Event.findAll({
        where: {
            userId: 1,
            status: 'active'
        }
    });

    return res.json(results);
});


router.post('/create', async (req, res) => {
    const { Event } = req.app.get("db").models;

    const { start_date, end_date, description } = req.body;
    // verifs
    // si pb data -> res.status(400).body("Invalid machin")
    // sinon

    const createEvent = { start_date, end_date, description };

    const created = await Event.create(createEvent);
    const message = `The event was created.`;
    return res.json({ message, created });
});

router.put('./events/:id', (req, res) => {
    const id = parseInt(req.params.id)
    const eventUpdated = { ...req.body, id: id }
    events = events.map(event => {
        return event.id === id ? eventUpdated : event
    })
    const message = `The event was updated.`
    res.json(success(message, eventUpdated))
})



router.delete('/delete', (req, res) => {
    //    res.send('Got a delete request at /user')
    const id = parseInt(req.params.id)
    const eventToDelete = events.find(event => event.id === id)
    events.filter(event => event.id !== id)
    if (!id) {
        return ('This event does not exist')
    } else {
        eventToDelete.splice(id)
        res.status(200).json('The event is now deleted.')
    }
});

module.exports = router;
